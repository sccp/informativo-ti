INFORMATIVO DA TI
=================

Blog em Wordpress da equipe de tecnologia da informação para criação de artigos diversos, com a finalidade de enviar newsletters com informações úteis, notificações de serviços, alertas e outras comunicações de utilidade para os usuários do Sport Club Corinthians Paulista.

Esse repositório contém o código fonte do blog, plugins e templates ajustados para imitar o ambiente de produção, seu uso é livre desde que citado a fonte. Logotipos, mascotes e nomes que podem estar presentes neste repositório são de propriedade do Sport Club Corinthians Paulista ou de seus respectivos fornecedores, seu uso deve obedecer as regulamentações aplicáveis.

*Equipe de Tecnologia de Informação*
Sport Club Corinthians Paulista
ti@sccorinthians.com.br
