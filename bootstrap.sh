#!/usr/bin/env bash

# Install Apache e MySQL
apt-get update
echo mysql-server mysql-server/root_password select "vagrant" | debconf-set-selections
echo mysql-server mysql-server/root_password_again select "vagrant" | debconf-set-selections
apt-get install -y apache2 mysql-server mysql-client php5 libapache2-mod-php5 php5-mysql

VHOST=$(cat <<EOF
<VirtualHost *:80>
    DocumentRoot "/vagrant/wordpress"
    ServerName localhost
    <Directory "/vagrant/wordpress">
        AllowOverride All
    </Directory>
</VirtualHost>
EOF
)

echo "${VHOST}" > /etc/apache2/sites-enabled/000-default
a2enmod rewrite
service apache2 restart
mysql -u root -p"vagrant" -e ";CREATE DATABASE IF NOT EXISTS informativoti;GRANT ALL ON informativoti.* TO informativo@'%' IDENTIFIED BY 'informativo'"
sed -i 's/127.0.0.1/0.0.0.0/g' /etc/mysql/my.cnf
service mysql restart

apt-get clean
